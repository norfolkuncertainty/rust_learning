//arg0.rs
fn main () {
    for arg in std::env::args() {
        println!("{}", arg)
    }

    let argvec: Vec<String> = std::env::args().skip(1).collect();
    if argvec.len() > 1 {
        println!("{:?}", argvec)
    }
}