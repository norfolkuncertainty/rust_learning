//arg1.rs
use std::env;

fn main() {
    let first = env::args().nth(1).expect("Please supply argument");
    let n: i32 = first.parse().expect("Not an Integer");
    println!("{}", n)
}