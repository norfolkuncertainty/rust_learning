//string4

fn main(){
// BAD
//    let s = "¡";
//    println!("{}", &s[0..1]);

  let text = "the red fox and the lazy dog";
  let words: Vec<&str> = text.split_whitespace().collect();
  println!("{:?}", words);

  let mut words2 = Vec::new();
  words2.extend(text.split_whitespace());
  println!("{:?}", words2);

  let stripped: String = text.chars()
    .filter(|ch| ! ch.is_whitespace()).collect();
  println!("{:?}", stripped);

}